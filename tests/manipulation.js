window.tests = window.tests || {};

tests.manipulation = {
	appendHTMLToBody: function(str) {
		return body.append("<div>" + (str || "html string") + "</div>");
	},
	appendElementToBody: function(text) {
		var el = document.createElement("div");
		el.innerHTML = text || "append element";
		return body.append(el);
	},
	appendInstanceToBody: function(text) {
		var el = document.createElement("div");
		el.innerHTML = text || "append instanced element";
		return body.append($(el));
	},
	prepend: function(str) {
		return body.prepend("<div>" + (str || "prepended string") + "</div>");
	}
};
